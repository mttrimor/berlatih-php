<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>


<?php

echo "<h3> Soal No 1 Tentukan Nilai <h3>";

 function tentukan_nilai($n)
{   
   if($n>=98 AND $n <=100){
      echo "$n => Sangat Baik <br>";
  }else if($n>=76 AND $n<=98){
       echo "$n => Baik<br>";
   }else if($n>=67 AND $n<=75){
         echo "$n => Cukup <br>";
    }else if($n >=43){
         echo "$n => Kurang <br>";
     }
 }



 //TEST CASES
 echo tentukan_nilai(98); //Sangat Baik
 echo tentukan_nilai(76); //Baik
 echo tentukan_nilai(67); //Cukup
 echo tentukan_nilai(43); //Kurang

echo "<h3> Soal No.2 Ubah Huruf <h3>";

function ubah_huruf($string){
    echo str_shuffle("$string");
    echo "<br>";
    }
    
    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu

echo"<h3> Soal No.3 Tukar Besar Kecil <h3>";

    function tukar_besar_kecil($string){
       $output = "";
       for ($i=0; $i < strlen($string); $i++){
           if(ctype_upper($string[$i])){
               $output .= strtolower($string[$i]);
           }else{
               $output .= strtoupper($string[$i]);
           }
       }
       return $output."<br>";
    }
        
        // TEST CASES
        echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
        echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
        echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
        echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
        echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
        
?>
</body>
</html>